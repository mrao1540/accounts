package com.org.accounts.service;

import com.org.accounts.model.AccountEntity;
import com.org.accounts.repository.AccountRepository;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    @Mock
    AccountRepository accountRepository;

    @InjectMocks AccountService accountService = new AccountServiceImpl();

    public static Stream<Arguments> provideDataForAccountId() {
        AccountEntity entity = new AccountEntity();
        entity.setAccountName("nameing");
        entity.setId(1l);
        entity.setPrevilageCustomer(true);

        AccountEntity entityNonPrev = new AccountEntity();
        entityNonPrev.setAccountName("nameing");
        entityNonPrev.setId(15l);
        entityNonPrev.setPrevilageCustomer(false);


        return Stream.of(Arguments.of(new Object[]{1l, Optional.of(entity)}, getJsonObject(entity).toString()),
                Arguments.of(new Object[]{15l, Optional.of(entityNonPrev)}, getJsonObject(entityNonPrev).toString()),
                Arguments.of(new Object[]{10l, Optional.empty()}, "No record found for Id: 10")
        );
    }

    public static Stream<Arguments> provideDataFindAccountBalanceById() {
        AccountEntity entity = new AccountEntity();
        entity.setAccountName("nameing");
        entity.setId(1l);
        entity.setBalance(10000L);
        return Stream.of(Arguments.of(new Object[]{1l, Optional.of(entity)}, "The Balance of requested account Id: 1 is 10000"),
                Arguments.of(new Object[]{10l, Optional.empty()}, "No record found for Id: 10")
        );
    }


    @ParameterizedTest
    @MethodSource("provideDataForAccountId")
    public void testFindByAccountId(Object[] input, String expected){
        long id = new Long(input[0].toString());
        Mockito.when(accountRepository.findById(id)).thenReturn((Optional<AccountEntity>) input[1]);
        assertEquals(accountService.findByAccountId(id), expected);
    }

    private static JSONObject getJsonObject(AccountEntity accountEntity) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", accountEntity.getId());
        jsonObject.put("accountName", accountEntity.getAccountName());
        jsonObject.put("prevlageCustomer",accountEntity.getPrevilageCustomer());
        return jsonObject;
    }

    @ParameterizedTest
    @MethodSource("provideDataFindAccountBalanceById")
    public void testFindAccountBalanceById(Object[] input,String expected){
        long id = new Long(input[0].toString());
        Mockito.when(accountRepository.findById(id)).thenReturn((Optional<AccountEntity>) input[1]);
        assertEquals(accountService.findAccountBalanceById(id), expected);
    }

}