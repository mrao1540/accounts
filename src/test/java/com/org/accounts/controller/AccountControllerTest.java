package com.org.accounts.controller;

import com.org.accounts.model.AccountEntity;
import com.org.accounts.service.AccountService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@WebMvcTest(AccountController.class)
public class AccountControllerTest {

    @MockBean
    AccountService accountService;
    @Autowired
    MockMvc mockMvc;

    @Test
    public void testFindAll() throws Exception {
        AccountEntity account = new AccountEntity();
        account.setId(1L);
        account.setAccountName("name1");
        AccountEntity account1 = new AccountEntity();
        account1.setId(2L);
        account1.setAccountName("name2");
        JSONArray array = new JSONArray();
        array.put(getJsonObject(account));
        array.put(getJsonObject(account1));

        Mockito.when(accountService.findAllAccounts()).thenReturn(array.toString());

        MvcResult result = mockMvc.perform(get("/getAccInfo"))
                .andExpect(status().isOk()).andReturn();

        String actualResult = result.getResponse().getContentAsString();
        Assertions.assertEquals(actualResult, array.toString());
    }


    @Test
    public void testFindById() throws Exception {
        AccountEntity account = new AccountEntity();
        account.setId(1L);
        account.setAccountName("name1");

        Mockito.when(accountService.findByAccountId(Mockito.anyLong())).thenReturn(getJsonObject(account).toString());

        MvcResult result = mockMvc.perform(get("/getAccInfo").param("accountId", String.valueOf(1)))
                .andExpect(status().isOk()).andReturn();

        String actualResult = result.getResponse().getContentAsString();
        Assertions.assertEquals(actualResult, getJsonObject(account).toString());
    }

    private JSONObject getJsonObject(AccountEntity accountEntity) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", accountEntity.getId());
        jsonObject.put("accountName", accountEntity.getAccountName());
        return jsonObject;
    }
}
