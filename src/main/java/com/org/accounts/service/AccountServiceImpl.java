package com.org.accounts.service;

import com.org.accounts.model.AccountEntity;
import com.org.accounts.repository.AccountRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Override
    public String findByAccountId(Long id) {
       Optional<AccountEntity> accountEntityOptional = accountRepository.findById(id);
        if(accountEntityOptional.isPresent()){
            AccountEntity accountEntity = accountEntityOptional.get();
            JSONObject jsonObject = getJsonObjectOfEntity(accountEntity);
            return jsonObject.toString();
        }else{
            return "No record found for Id: "+ id;
        }
    }

    private JSONObject getJsonObjectOfEntity(AccountEntity accountEntity) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", accountEntity.getId());
        jsonObject.put("accountName", accountEntity.getAccountName() );
        jsonObject.put("address", accountEntity.getAddress());
        jsonObject.put("balance", accountEntity.getBalance());
        jsonObject.put("accountType", accountEntity.getAccountType());
        jsonObject.put("createdDate", accountEntity.getCreatedDate());
        jsonObject.put("createdBy", accountEntity.getCreatedBy());
        jsonObject.put("updatedDate", accountEntity.getUpdatedDate());
        jsonObject.put("updatedBy", accountEntity.getUpdatedBy());
        jsonObject.put("prevlageCustomer", accountEntity.getPrevilageCustomer());
        return jsonObject;
    }


    @Override
    public String findAllAccounts() {
        List<AccountEntity> accountEntityList = accountRepository.findAll();
        JSONArray jsonArray = new JSONArray();
        accountEntityList.forEach(account -> jsonArray.put(getJsonObjectOfEntity(account)));
        return jsonArray.toString();
    }

    @Override
    public String createAccount(AccountEntity account) {
        if(account.getId() == null) {
            account.setCreatedDate(Instant.now());
            account.setCreatedBy(-1L);
            return getJsonObjectOfEntity(accountRepository.save(account)).toString();
        }else{
            return "Invalid request";
        }
    }

    @Override
    public String updateAccount(AccountEntity account) {
        Optional<AccountEntity> entityOptional = accountRepository.findById(account.getId());
        if(entityOptional.isPresent()){
            AccountEntity entity = entityOptional.get();
            entity.setAccountName(account.getAccountName());
            entity.setAccountType(account.getAccountType());
            entity.setAddress(account.getAddress());
            entity.setBalance(account.getBalance());
            entity.setUpdatedDate(Instant.now());
            entity.setUpdatedBy(-1L);
            accountRepository.save(entity);
            return getJsonObjectOfEntity(entity).toString();
        }else{
            return "No record found for Id: "+account.getId();
        }
    }

    @Override
    public String findAccountBalanceById(Long id) {
        Optional<AccountEntity> entityOptional = accountRepository.findById(id);
        if(entityOptional.isPresent()){
            return "The Balance of requested account Id: "+ id +" is " +entityOptional.get().getBalance();
        }else{
            return "No record found for Id: "+id;
        }
    }


}
