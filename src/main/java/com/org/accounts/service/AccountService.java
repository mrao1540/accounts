package com.org.accounts.service;

import com.org.accounts.model.AccountEntity;

public interface AccountService {

    String findByAccountId(Long id);
    String findAllAccounts();
    String createAccount(AccountEntity account);
    String updateAccount(AccountEntity account);
    String findAccountBalanceById(Long id);
}
