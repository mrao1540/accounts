package com.org.accounts.controller;

import com.org.accounts.model.AccountEntity;
import com.org.accounts.service.AccountService;
import com.org.accounts.service.AccountServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class AccountController {

    @Autowired
    AccountService accountService;

    @GetMapping("/getAccInfo")
    public String getAccounts(@RequestParam(value = "accountId", required = false) Long accountId){
        if(accountId != null){
            return accountService.findByAccountId(accountId);
        }else{
           return  accountService.findAllAccounts();
        }
    }

    @PostMapping("/createAccount")
    public String createAccount(@RequestBody AccountEntity entity){
        return accountService.createAccount(entity);
    }

    @PutMapping("/updateAccount")
    public String updateAccount(@RequestBody AccountEntity entity){
        return accountService.updateAccount(entity);
    }

    @GetMapping("/getBalance/{accountId}")
    public String findAccountBalanceById(@PathVariable(value = "accountId") Long id){
        return accountService.findAccountBalanceById(id);
    }
}
