package com.org.accounts.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "ACCOUNT")
@Getter
@Setter
public class AccountEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "ACCOUNT_NAME")
    private String accountName;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "BALANCE")
    private Long balance;

    @Column(name = "ACCOUNT_TYPE")
    private String accountType;

    @Column(name = "CREATED_DATE")
    private Instant createdDate;

    @Column(name = "CREATED_BY")
    private Long createdBy;

    @Column(name = "UPDATED_DATE")
    private Instant updatedDate;

    @Column(name = "UPDATED_BY")
    private Long updatedBy;

    @Column(name="PREVILAGE_CUSTOMER")
    private Boolean previlageCustomer;

}
